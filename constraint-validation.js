// TODO
const selectEl = document.getElementById('contact-kind');

const setSelectValidty = function () {
    if (selectEl.value === 'choose') {
        selectEl.setCustomValidity("Must select an option");
    } else {
        selectEl.setCustomValidity('');
    }
}

setSelectValidty();
selectEl.addEventListener('change', setSelectValidty);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;
    Array.from(inputs).forEach(input => {
        const small = input.parentElement.querySelector('small');

        if (!input.checkValidity()) {
          formIsValid = false;
          small.innerText = input.validationMessage;
        } else {
          small.innerText = ''
        }
    });
    //6:50-7pm hour on week7 video lookup

    if (!formIsValid) {
    e.preventDefault();
    }
});