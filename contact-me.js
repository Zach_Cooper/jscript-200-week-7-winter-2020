// TODO

//
const selectEl = document.getElementById('contact-reason');

const setSelectValidity = function() {
    if (selectEl.value == 'choose') {
        selectEl.setCustomValidity('You must select an option')
        return;
    }
    if (selectEl.value === 'business'){
        document.querySelectorAll('.business').forEach(el => {
            el.classList.remove('hide')
        })
        document.querySelectorAll('.technical').forEach(el => {
            el.classList.add('hide')
        })
    } else if (selectEl.value === 'technical') {
        document.querySelectorAll('.technical').forEach(el => {
            el.classList.add('hide')
        })
        document.querySelectorAll('.business').forEach(el => {
            el.classList.add('hide')
        })
    }
}

setSelectValidity();
selectEl.addEventListner('change', setSelectValidity);

// Set validation for the following items: Name, Email, Message

const form = document.querySelector('form');

////Add Event Listener on the name submit section
form.addEventListener('submit', function(e) {
  const firstName = document.getElementById('first-name');
  if (firstName.value.length < 2) {
    e.prevenDefault()
    firstName.classList.add('invalid')

    const small = firstName.parentElement.querySelector('small');
    small.innerText = ('ERROR!!! This field must have at least 2 characters.')
   }
});

//Add Event Listener on the message part
form.addEventListener('submit', function(e) {
    const message = document.getElementById('message');
    if (message.value.length < 10) {
        e.preventDefault()
        message.classList.add('invalid')

      const small = message.parentElement.querySelector('small');
      small.innerText = ('ERROR!!! This field must be at least 10 characters.')
    }
});

//Add Event Listener on the email submission
form.addEventListener('submit', function(e) {
const email = document.getElementById('email');
const emailText = document.getElementById('email').value;
const regex = /\w+@\w+\.\w+/g;
    if (! emailText.match(regex) ) {
        e.prevenDefault()
        email.classList.add('invalid')

        const small = email.parentElement.querySelector('small');
        small.innerText = ('ERROR!!! The field must contain the correct e-mail address.')
    }
});


