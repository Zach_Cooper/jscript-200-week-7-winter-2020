/**
 * Car class
 * @constructor
 * @param {String} model
 */

 class Car {
     constructor(model) {
         this.currentSpeed = 0;
         this.model = model;
     }
     accelerate() {
         this.currentSpeed++;
     }
     brake() {
         this.currentSpeed--;
     }
     toString() {
         console.log(`The ${this.model} is going ${this.currentSpeed} mph!!`)
     }
 }
//Declare the car and call the instance
const ferrari = new Car('Ferrari');
ferrari.accelerate();
ferrari.accelerate();
ferrari.brake();
ferrari.toString();


//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
  //accelerate() {
      //let speed = 0;
      //console
  //}
//const ferrari = new Car('Ferrari');
//ferrari.accelerate();
//ferrari.accelerate();
//ferrari.brake();
//ferrari.toString();

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */
class ElectricCar extends Car {
    constructor(model){
        super(model);
        this.motor = 'electric';
    }
    accelerate() {
        super.accelerate();
        super.accerlerate();
    }
    toString() {
        console.log(`The ${this.model} is going ${thiscurrentSpeed} with a electric engine!!`)
    }
}

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const teslaElectric = new ElectricCar('Tesla');
teslaElectric.accelerate();
teslaElectric.accerlate();
teslaElectric.brake();

teslaElectric.toString();